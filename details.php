<?php include"includes/header.php"; ?>
<?php include"includes/db.php"; ?>
<?php include"functions.php"; ?>
<?php
date_default_timezone_set('Africa/Nairobi');
if(isset($_GET['menu_id'])){
    $menu_id = $_GET['menu_id'];
    $query = "select * from menu_table where menu_id=$menu_id";
    $retrieve_items = mysqli_query($connection,$query);

    while($row = mysqli_fetch_assoc($retrieve_items)){
        $menu_id = $row['menu_id'];
        $day = $row['date'];
        $item = $row['item'];
        $price = $row['price'];
        $image = $row['image'];
        $time = $row['delivery_time'];
        $max_orders = $row['max_orders'];
    }   
}

?>

<body style="background-color: #061a06">
        <!-- NavBar -->
        <?php include"includes/nav.php"?>
        <!-- product details section -->

    <div class="container">
            <div class="row">
                    <div class="col-sm-12">
                        <div class="bg-white p-3 w-75 mx-auto border border-primary rounded">
                            <h1 class="display-5 text-center"><?php echo strtoupper($day)?> FOOD DETAILS</h1>
                            <hr style="height:2px; background-color: #061a06">
                            <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="color: rgb(75, 75, 209)">Informations</th>
                                            <th style="color: rgb(75, 75, 209)">Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Food Dish</th>
                                            <td><?php echo $item?></td>
                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">Delivery Time</th>
                                            <td><?php echo $time?>pm</td>
                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">Remaining Orders</th>
                                            <td><?php echo $max_orders?></td>
                                            <td>
                                               <?php include"includes/modal.php"?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                        </div>
                    </div>
            </div>        

     </div>
     <!-- footer -->
     <div class="card text-center">
            <div class="card-body">
                <h4 class="card-header">Be served By the Best</h4>
                <p class="card-text">We guarantee safe,fast and timely delivery of your food
                    <br> for more information contact us on
                    <strong>0703469646/0772067628</strong>
                </p>
                <a href="#" class="btn" style="background-color: #061a06; color: white;">Find Out About Us</a>
            </div>
            <script src="jquery-3.3.1.min.js"></script>
            <script src="popper.js"></script>
            <script src="js/bootstrap.js"></script>
</body>