<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Josco's|Restaurant</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>

<body style="background-color: #061a06">
    <!-- NavBar -->
    <nav class="navbar navbar-expand-sm navbar-light sticky-top  " style="background-color: #f7fafc; ">
        <div class="container">
            <a class="navbar-brand  " href="">Josco</a>
            <button class="navbar-toggler rounded-circle " data-toggle="collapse" data-target="#navbarnav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarnav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Admin</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.php">About-us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- footer -->
    <div class="container">
        <div class="card text-center">
            <div class="card-body">
                <h4 class="card-header">Be served By the Best</h4>
                <p class="card-text">We guarantee safe,fast and timely delivery of your food
                    <br> for more information contact us on
                    <strong>0703469646/0772067628</strong>
                </p>
                <a href="#" class="btn" style="background-color: #061a06; color: white;">Find Out About Us</a>
            </div>
        </div>
    </div>
        <nav>
            <ul class="pagination justify-content-center">
                <li class="page-item active">
                    <a class="page-link" style="background-color: #061a06"  href="index.php">Home</a>
                </li>
                <li class="page-item active">
                    <a class="page-link" style="background-color: white; color: black;" href="about-us.php">About</a>
                </li>
            </ul>
        </nav>
    
    <script src="jquery-3.3.1.min.js"></script>
    <script src="popper.js"></script>
    <script src="js/bootstrap.js"></script>
</body>