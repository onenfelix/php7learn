-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2019 at 08:02 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `joan1`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_table`
--

CREATE TABLE `menu_table` (
  `date` char(255) NOT NULL,
  `item` char(255) NOT NULL,
  `price` int(10) NOT NULL,
  `menu_id` int(3) NOT NULL,
  `image` text NOT NULL,
  `delivery_time` time NOT NULL,
  `max_orders` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_table`
--

INSERT INTO `menu_table` (`date`, `item`, `price`, `menu_id`, `image`, `delivery_time`, `max_orders`) VALUES
('Monday', 'Irish and Spaghetti', 1500, 1, 'food', '04:05:00', 5),
('Tuesday', 'Posho and Beans', 1500, 2, 'food', '10:08:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `settings_table`
--

CREATE TABLE `settings_table` (
  `max_orders` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `order_id` int(3) NOT NULL,
  `customer_name` char(255) NOT NULL,
  `hall` char(255) NOT NULL,
  `room` int(3) NOT NULL,
  `contact` int(15) NOT NULL,
  `date` date NOT NULL,
  `day` char(255) NOT NULL,
  `item` char(255) NOT NULL,
  `order_status` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_orders`
--

INSERT INTO `user_orders` (`order_id`, `customer_name`, `hall`, `room`, `contact`, `date`, `day`, `item`, `order_status`) VALUES
(1, 'juma', 'Alexander', 8, 0, '2019-02-25', 'Monday', 'Posho and Beans', 'unfulfilled'),
(2, 'onen', 'Alexander', 18, 7665, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(3, 'abitech', 'James Ogoola', 18, 4, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(4, 'abra', 'James Ogoola', 8, 877, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(5, 'Wataba isaac', 'Proscovia Njuki', 90, 2, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(6, 'sacred heart ss gulu', 'James Ogoola', 12, 4, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(7, 'ogen', 'Shaule', 12, 4, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(8, 'yuutuut', 'Proscovia Njuki', 12, 65774, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(9, 'onen felix', 'Proscovia Njuki', 8, 6, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(10, 'tyy', 'Alexander', 90, 5, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(11, 'yrurur', 'Proscovia Njuki', 19, 9, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(12, 'abalo', 'Alexander', 56, 45, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(13, 'sgfswg', 'James Ogoola', 3, 43435, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(14, 'fsfgs', 'James Ogoola', 23, 2345, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled'),
(15, 'op', 'Shaule', 23, 5556, '2019-03-08', 'Friday', 'Posho and Beans', 'unfulfilled');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_table`
--
ALTER TABLE `menu_table`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `user_orders`
--
ALTER TABLE `user_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_table`
--
ALTER TABLE `menu_table`
  MODIFY `menu_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_orders`
--
ALTER TABLE `user_orders`
  MODIFY `order_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
