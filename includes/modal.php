<!-- ORDER MODAL TRIGGER -->
<button class="btn btn-info" style="background-color: #061a06; color: white;" data-toggle="modal" data-target="#myModal">Order</button>
<div id="result"></div>
<!-- ORDER MODAL -->
<div class="modal" id="myModal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Order</h5>
        <button class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <!-- body -->
        <form action="" id="orderform" method="post" enctype="multipart/form-data" >
            <div class="form-group">
                <label for="username">Enter Your Name</label>
                <input type="text" id="vname" placeholder="Username" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="hall">Hall of residence</label>
                    <select id="vhall" name="hall" class="form-control">
                        <option value="Alexander">Alexander</option>
                        <option value="James Ogoola">Ogoola</option>
                        <option value="Proscovia Njuki">Njuki</option>
                        <option value="Shaule">Non Resident</option>
                    </select>
            </div>
            <div class="form-group">
                <label for="password">Room Number</label>
                <input id="vroom" type="text" placeholder="Room Number" class="form-control" name="room">
            </div>
            <div class="form-group">
                <label for="password">Contact</label>
                <input id="vcontact" type="number" placeholder="Phone Number" class="form-control" name="contact">
            </div>

        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="vorder" data-dismiss="modal" name="order">Order</button>
        
    </div>
</div>
</div>
</div>