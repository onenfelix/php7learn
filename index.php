<?php include"includes/header.php"; ?>
<?php include"includes/db.php"; ?>
<?php include"functions.php"; ?>
<?php
date_default_timezone_set('Africa/Nairobi');


?>

    




<body style="background-color: #061a06">

    <!-- NavBar -->

    <?php include"includes/nav.php"?>
    <!-- Main Content -->

    <div class="container">
        <!-- menu section -->
        <div class="row">
            <div class="col-sm-12">
                <div class="bg-white p-3 w-75 mx-auto border border-primary rounded">
                    <h1 class="display-5 text-center">JOSCO'S RESTAURANT MENU</h1>
                    <hr>
                    <div class="row">

                     <!--retrive menu items-->
                <?php
                    $query = "select * from menu_table";
                    $retrieve_items = mysqli_query($connection,$query);

                    while($row = mysqli_fetch_assoc($retrieve_items)){
                        $menu_id = $row['menu_id'];
                        $day = $row['date'];
                        $item = $row['item'];
                        $price = $row['price'];
                        $image = $row['image'];


                   
                    
                ?>
                
                        <div class="col-md-6">
                            <div class="card-body text-center">
                                <h4 class="card-title"><?php echo $day ?> </h4>
                                <p class="card-text"><?php echo $item ?> </p>
                                <p class="card-text">At <?php echo $price ?>  /=</p>
                                <!-- ORDER MODAL TRIGGER -->
                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Order</button>
                                <a href="details.php?menu_id=<?php echo $menu_id?>" class="btn btn-success btn-inline">Details</a>
                                    <div id="result"></div>
                                <!-- ORDER MODAL -->
                                <div class="modal" id="myModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Order</h5>
                                                <button class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- body -->
                                                <form action="" id="orderform" method="post" enctype="multipart/form-data" >
                                                    <div class="form-group">
                                                        <label for="username">Enter Your Name</label>
                                                        <input type="text" id="vname" placeholder="Username" class="form-control" name="name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="hall">Hall of residence</label>
                                                            <select id="vhall" name="hall" class="form-control" required >
                                                                <option value="Alexander">Alexander</option>
                                                                <option value="James Ogoola">Ogoola</option>
                                                                <option value="Proscovia Njuki">Njuki</option>
                                                                <option value="Shaule">Non Resident</option>
                                                            </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Room Number</label>
                                                        <input id="vroom" type="number" placeholder="Room Number" class="form-control" name="room" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Contact</label>
                                                        <input id="vcontact" type="number" placeholder="Phone Number" class="form-control" name="contact" required>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary" id="vorder" data-dismiss="modal" name="order">Order</button>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <a href="#" class="btn btn-success btn-inline">Order</a> -->
                                <img class="card-img mt-2" src="imgs/<?php echo $image?>.jpg" alt="Card image cap">
                            </div>
                            <hr>
                        </div>

                   <?php }?>

                   <!-- <?php ?>-->
                
                        <?php

                    // if(isset($_POST['name'])){
                    //     echo "<h4>Thank you for submission!<h4>";
                    //     echo "<br>Your Name: <b>".$_POST["name"]."</b>";
                    // }
                        
                
              



       # placing user orders 
                   
        if(isset($_POST['name'])){

         $customer_name =$_POST['name'];
        $hall = $_POST['hall'];
        $room = $_POST['room'];
        $contact = $_POST['contact'];
        $date = date('Y-m-d H:i:s');
         $day = date('l');

       // echo $customer_name;
       // echo $room;
       // echo $contact;


         $alpha="insert into user_orders(customer_name,hall,room,contact,date,day,item,order_status) ";

         $omega="values('$customer_name','{$hall}',$room,$contact,'{$date}','{$day}','{$item}','unfulfilled')";
         $query=$alpha.$omega;

        $place_order_query = mysqli_query($connection,$query);
         if($place_order_query){
             echo "yes";
        }else{
             echo "no";
         }

         confirmquery($place_order_query);

         }
                        
        ?> 

                        

                        
                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="card text-center">
        <div class="card-body">
            <h4 class="card-header">Be served By the Best</h4>
            <p class="card-text">We guarantee safe,fast and timely delivery of your food
                <br> for more information contact us on
                <strong>0703469646/0772067628</strong>
            </p>
            <a href="about-us.php" class="btn btn-danger">Find Out About Us</a>
        </div>
    </div>


</body>

</html>